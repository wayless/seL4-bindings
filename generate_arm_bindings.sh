#!/bin/bash
bindgen --use-core seL4/stage/arm/am335x/include/sel4/sel4_arch/syscalls.h -- -IseL4/include/generated/ -IseL4/stage/arm/am335x/include/ > src/bindings/arm/bindings.rs
bindgen --use-core seL4/stage/arm/am335x/include/sel4/bootinfo.h -- -IseL4/include/generated/ -IseL4/stage/arm/am335x/include/ > src/bindings/arm/bindings-bootinfo.rs
