#!/bin/bash
bindgen --use-core seL4/stage/x86/pc99/include/sel4/sel4_arch/syscalls.h -- -IseL4/include/generated/ -IseL4/stage/x86/pc99/include/ > src/bindings/x86/bindings.rs
bindgen --use-core seL4/stage/x86/pc99/include/sel4/bootinfo.h -- -IseL4/include/generated/ -IseL4/stage/x86/pc99/include/ > src/bindings/x86/bindings-bootinfo.rs
