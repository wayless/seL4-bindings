/*  Wayless, an operating system built on seL4
    Copyright (C) 2017 Waylon Cude

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#![no_std]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
extern crate libc;
pub mod seL4 {
    #[cfg(target_arch = "x86_64")]
    include!("bindings/x86/bindings.rs");
    #[cfg(target_arch = "arm")]
    include!("bindings/arm/bindings.rs");
}
pub mod bootinfo {
    #[cfg(target_arch = "x86_64")]
    include!("bindings/x86/bindings-bootinfo.rs");
    #[cfg(target_arch = "arm")]
    include!("bindings/arm/bindings-bootinfo.rs");
}
//pub mod seL4_client {
//    include!(concat!(env!("OUT_DIR"), "/bindings-client.rs"));
//}
mod std {
    pub mod os {
        pub mod raw {
            use libc;
            pub type c_char = libc::c_char;
            pub type c_uchar = libc::c_uchar;
            pub type c_ushort = libc::c_ushort;
            pub type c_uint = libc::c_uint;
            pub type c_ulong = libc::c_ulong;
            pub type c_short = libc::c_short;
            pub type c_int = libc::c_int;
            pub type c_long = libc::c_long;
        }
    }
}
//Hand-ported syscalls
pub mod seL4_rust {
    use super::seL4;
    extern {
        pub fn seL4_X86_IOPort_Out32(cap: seL4::seL4_X86_IOPort,port: u16,data: u32) -> u32;
        pub fn seL4_X86_IOPort_In32(cap: seL4::seL4_X86_IOPort,port: u16)-> [u32;2];
        pub fn seL4_CNode_Mint(
            _service: seL4::seL4_CNode,
            dest_index: seL4::seL4_Word,
            dest_depth: u8,
            src_root: seL4::seL4_CNode,
            src_index: seL4::seL4_Word,
            src_depth: u8,
            rights: seL4::seL4_CapRights_t,
            badge: seL4::seL4_CapData_t
            ) -> isize;
    } 
}
